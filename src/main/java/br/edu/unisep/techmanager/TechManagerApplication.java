package br.edu.unisep.techmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class TechManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechManagerApplication.class, args);
	}

}
