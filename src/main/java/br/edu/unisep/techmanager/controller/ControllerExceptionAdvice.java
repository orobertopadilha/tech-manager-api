package br.edu.unisep.techmanager.controller;

import br.edu.unisep.techmanager.domain.dto.error.ResponseErrorDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerExceptionAdvice {

    @ExceptionHandler({IllegalArgumentException.class, NullPointerException.class})
    public ResponseEntity<ResponseErrorDto> handleValidationErrors(Exception exception) {
        return ResponseEntity.badRequest().body(new ResponseErrorDto(exception.getMessage()));
    }
}
