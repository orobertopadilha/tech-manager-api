package br.edu.unisep.techmanager.controller.equipment;

import br.edu.unisep.techmanager.domain.dto.equipment.EquipmentDto;
import br.edu.unisep.techmanager.domain.dto.equipment.RegisterEquipmentDto;
import br.edu.unisep.techmanager.domain.usecase.equipment.FindAllEquipmentsUseCase;
import br.edu.unisep.techmanager.domain.usecase.equipment.RegisterEquipmentUseCase;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/equipment")
public class EquipmentController {

    private final FindAllEquipmentsUseCase findAllEquipmentsUseCase;
    private final RegisterEquipmentUseCase registerEquipmentUseCase;

    @GetMapping
    public ResponseEntity<List<EquipmentDto>> findAll() {
        var equipments = findAllEquipmentsUseCase.execute();
        return ResponseEntity.ok(equipments);
    }

    @PostMapping
    public ResponseEntity save(@RequestBody RegisterEquipmentDto equipment) {
        registerEquipmentUseCase.execute(equipment);
        return ResponseEntity.ok().build();
    }

}
