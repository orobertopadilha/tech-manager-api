package br.edu.unisep.techmanager.controller.equipment;

import br.edu.unisep.techmanager.domain.dto.equipment.EquipmentTypeDto;
import br.edu.unisep.techmanager.domain.usecase.equipment.FindAllEquipmentTypesUseCase;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/equipment-type")
public class EquipmentTypeController {

    private final FindAllEquipmentTypesUseCase findAllEquipmentTypesUseCase;

    @GetMapping
    public ResponseEntity<List<EquipmentTypeDto>> findAll() {
        var equipmentTypes = findAllEquipmentTypesUseCase.execute();
        return ResponseEntity.ok(equipmentTypes);
    }

}
