package br.edu.unisep.techmanager.controller.reservation;

import br.edu.unisep.techmanager.domain.dto.reservation.RegisterReservationDto;
import br.edu.unisep.techmanager.domain.dto.reservation.ReservationDto;
import br.edu.unisep.techmanager.domain.usecase.reservation.FindAllReservationsUseCase;
import br.edu.unisep.techmanager.domain.usecase.reservation.FindMyReservationsUseCase;
import br.edu.unisep.techmanager.domain.usecase.reservation.RegisterReservationUseCase;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/reservation")
public class ReservationController {

    private final FindAllReservationsUseCase findAllReservationsUseCase;
    private final FindMyReservationsUseCase findMyReservationsUseCase;
    private final RegisterReservationUseCase registerReservationUseCase;

    @GetMapping
    public ResponseEntity<List<ReservationDto>> findAll() {
        var reservations = findAllReservationsUseCase.execute();
        return ResponseEntity.ok(reservations);
    }

    @GetMapping("/my-reservations")
    public ResponseEntity<List<ReservationDto>> findMyReservations(@RequestHeader("auth-user-id") Integer userId) {
        var reservations = findMyReservationsUseCase.execute(userId);
        return ResponseEntity.ok(reservations);
    }

    @PostMapping
    public ResponseEntity register(@RequestBody RegisterReservationDto registerReservation,
                                   @RequestHeader("auth-user-id") Integer userId) {
        registerReservationUseCase.execute(registerReservation, userId);
        return ResponseEntity.ok().build();
    }

}
