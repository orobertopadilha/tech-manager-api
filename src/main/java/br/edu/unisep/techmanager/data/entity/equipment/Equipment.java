package br.edu.unisep.techmanager.data.entity.equipment;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "equipments")
public class Equipment {

    @Id
    @Column(name = "equipment_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "type_id")
    private EquipmentType type;

}
