package br.edu.unisep.techmanager.data.entity.equipment;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "equipment_types")
public class EquipmentType {

    @Id
    @Column(name = "type_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "type_name")
    private String name;

}
