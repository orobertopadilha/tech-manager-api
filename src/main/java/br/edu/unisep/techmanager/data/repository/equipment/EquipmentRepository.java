package br.edu.unisep.techmanager.data.repository.equipment;

import br.edu.unisep.techmanager.data.entity.equipment.Equipment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EquipmentRepository extends JpaRepository<Equipment, Integer> {
}
