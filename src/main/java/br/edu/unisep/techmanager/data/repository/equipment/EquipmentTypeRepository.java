package br.edu.unisep.techmanager.data.repository.equipment;

import br.edu.unisep.techmanager.data.entity.equipment.EquipmentType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EquipmentTypeRepository extends JpaRepository<EquipmentType, Integer> {
}
