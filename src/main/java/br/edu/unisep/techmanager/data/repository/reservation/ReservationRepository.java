package br.edu.unisep.techmanager.data.repository.reservation;

import br.edu.unisep.techmanager.data.entity.reservation.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Integer> {

    @Query("from Reservation where equipment.id = :equipmentId and date = :date")
    Optional<Reservation> findByDateAndEquipment(Integer equipmentId, LocalDate date);

    @Query("from Reservation where user = :userId order by date desc")
    List<Reservation> findByUser(Integer userId);
}
