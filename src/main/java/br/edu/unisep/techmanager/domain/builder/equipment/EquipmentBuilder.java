package br.edu.unisep.techmanager.domain.builder.equipment;

import br.edu.unisep.techmanager.data.entity.equipment.Equipment;
import br.edu.unisep.techmanager.data.entity.equipment.EquipmentType;
import br.edu.unisep.techmanager.domain.dto.equipment.EquipmentDto;
import br.edu.unisep.techmanager.domain.dto.equipment.RegisterEquipmentDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class EquipmentBuilder {

    public List<EquipmentDto> from(List<Equipment> equipments) {
        return equipments.stream().map(this::from).collect(Collectors.toList());
    }

    public EquipmentDto from(Equipment equipment) {
        return new EquipmentDto(
                equipment.getId(),
                equipment.getName(),
                equipment.getType().getName()
        );
    }

    public Equipment from(RegisterEquipmentDto registerEquipment) {
        var equipment = new Equipment();
        equipment.setName(registerEquipment.getName());
        equipment.setType(new EquipmentType());
        equipment.getType().setId(registerEquipment.getType());

        return equipment;
    }
}
