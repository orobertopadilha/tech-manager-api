package br.edu.unisep.techmanager.domain.builder.equipment;

import br.edu.unisep.techmanager.data.entity.equipment.EquipmentType;
import br.edu.unisep.techmanager.domain.dto.equipment.EquipmentTypeDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class EquipmentTypeBuilder {

    public List<EquipmentTypeDto> from(List<EquipmentType> types) {
        return types.stream().map(this::from).collect(Collectors.toList());
    }

    public EquipmentTypeDto from(EquipmentType equipmentType) {
        return new EquipmentTypeDto(
                equipmentType.getId(),
                equipmentType.getName()
        );
    }
}
