package br.edu.unisep.techmanager.domain.builder.reservation;

import br.edu.unisep.techmanager.data.entity.equipment.Equipment;
import br.edu.unisep.techmanager.data.entity.reservation.Reservation;
import br.edu.unisep.techmanager.domain.dto.reservation.RegisterReservationDto;
import br.edu.unisep.techmanager.domain.dto.reservation.ReservationDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ReservationBuilder {

    public List<ReservationDto> from(List<Reservation> reservations) {
        return reservations.stream().map(this::from).collect(Collectors.toList());
    }

    public ReservationDto from(Reservation reservation) {
        return new ReservationDto(
                reservation.getId(),
                "",
                reservation.getEquipment().getName(),
                reservation.getEquipment().getType().getName(),
                reservation.getDate()
        );
    }

    public Reservation from(RegisterReservationDto registerReservation, Integer userId) {
        var equipment = new Equipment();
        equipment.setId(registerReservation.getEquipment());

        var reservation = new Reservation();
        reservation.setDate(registerReservation.getDate());
        reservation.setEquipment(equipment);
        reservation.setUser(userId);

        return reservation;
    }

}
