package br.edu.unisep.techmanager.domain.dto.equipment;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class EquipmentTypeDto {

    private final Integer id;

    private final String name;

}
