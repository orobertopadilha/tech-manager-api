package br.edu.unisep.techmanager.domain.dto.equipment;

import lombok.Data;

@Data
public class RegisterEquipmentDto {

    private Integer type;

    private String name;

}
