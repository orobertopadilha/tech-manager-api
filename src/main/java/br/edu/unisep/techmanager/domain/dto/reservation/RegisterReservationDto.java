package br.edu.unisep.techmanager.domain.dto.reservation;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;

@Getter
@AllArgsConstructor
public class RegisterReservationDto {

    private final Integer equipment;

    private final LocalDate date;
}
