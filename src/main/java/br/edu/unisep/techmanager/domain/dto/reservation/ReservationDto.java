package br.edu.unisep.techmanager.domain.dto.reservation;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;

@Getter
@AllArgsConstructor
public class ReservationDto {

    private final Integer id;

    private final String user;

    private final String equipment;

    private final String equipmentType;

    private final LocalDate date;

}
