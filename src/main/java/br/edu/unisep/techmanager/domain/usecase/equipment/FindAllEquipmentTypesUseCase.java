package br.edu.unisep.techmanager.domain.usecase.equipment;

import br.edu.unisep.techmanager.data.repository.equipment.EquipmentTypeRepository;
import br.edu.unisep.techmanager.domain.builder.equipment.EquipmentTypeBuilder;
import br.edu.unisep.techmanager.domain.dto.equipment.EquipmentTypeDto;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindAllEquipmentTypesUseCase {

    private final EquipmentTypeBuilder equipmentTypeBuilder;
    private final EquipmentTypeRepository equipmentTypeRepository;

    public List<EquipmentTypeDto> execute() {
        var equipmentTypes = equipmentTypeRepository.findAll(
                Sort.by(Sort.Order.asc("name")));
        return equipmentTypeBuilder.from(equipmentTypes);
    }
}
