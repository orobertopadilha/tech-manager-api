package br.edu.unisep.techmanager.domain.usecase.equipment;

import br.edu.unisep.techmanager.data.repository.equipment.EquipmentRepository;
import br.edu.unisep.techmanager.domain.builder.equipment.EquipmentBuilder;
import br.edu.unisep.techmanager.domain.dto.equipment.EquipmentDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindAllEquipmentsUseCase {

    private final EquipmentBuilder equipmentBuilder;
    private final EquipmentRepository equipmentRepository;

    public List<EquipmentDto> execute() {
        var equipments = equipmentRepository.findAll();
        return equipmentBuilder.from(equipments);
    }

}
