package br.edu.unisep.techmanager.domain.usecase.equipment;

import br.edu.unisep.techmanager.data.repository.equipment.EquipmentRepository;
import br.edu.unisep.techmanager.data.repository.equipment.EquipmentTypeRepository;
import br.edu.unisep.techmanager.domain.builder.equipment.EquipmentBuilder;
import br.edu.unisep.techmanager.domain.dto.equipment.RegisterEquipmentDto;
import br.edu.unisep.techmanager.domain.validator.equipment.EquipmentValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import static br.edu.unisep.techmanager.domain.validator.ValidationMessages.MESSAGE_INVALID_EQUIPMENT_TYPE;

@Service
@AllArgsConstructor
public class RegisterEquipmentUseCase {

    private EquipmentRepository equipmentRepository;
    private EquipmentTypeRepository equipmentTypeRepository;

    private EquipmentValidator validator;
    private EquipmentBuilder builder;

    public void execute(RegisterEquipmentDto equipment) {
        validator.validate(equipment);

        var equipmentType = equipmentTypeRepository.findById(equipment.getType());
        if (equipmentType.isEmpty()) {
            throw new IllegalArgumentException(MESSAGE_INVALID_EQUIPMENT_TYPE);
        }

        equipmentRepository.save(builder.from(equipment));
    }

}
