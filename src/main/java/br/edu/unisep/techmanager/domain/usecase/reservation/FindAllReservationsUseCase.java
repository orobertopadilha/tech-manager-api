package br.edu.unisep.techmanager.domain.usecase.reservation;

import br.edu.unisep.techmanager.data.repository.reservation.ReservationRepository;
import br.edu.unisep.techmanager.domain.builder.reservation.ReservationBuilder;
import br.edu.unisep.techmanager.domain.dto.reservation.ReservationDto;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindAllReservationsUseCase {

    private final ReservationBuilder reservationBuilder;
    private final ReservationRepository reservationRepository;

    public List<ReservationDto> execute() {
        var reservations = reservationRepository.findAll(Sort.by(Sort.Order.desc("date")));
        return reservationBuilder.from(reservations);
    }

}
