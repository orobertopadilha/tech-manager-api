package br.edu.unisep.techmanager.domain.usecase.reservation;

import br.edu.unisep.techmanager.data.repository.equipment.EquipmentRepository;
import br.edu.unisep.techmanager.data.repository.reservation.ReservationRepository;
import br.edu.unisep.techmanager.domain.builder.reservation.ReservationBuilder;
import br.edu.unisep.techmanager.domain.dto.reservation.RegisterReservationDto;
import br.edu.unisep.techmanager.domain.validator.reservation.ReservationValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import static br.edu.unisep.techmanager.domain.validator.ValidationMessages.MESSAGE_EQUIPMENT_ALREADY_RESERVED;
import static br.edu.unisep.techmanager.domain.validator.ValidationMessages.MESSAGE_INVALID_EQUIPMENT;

@Service
@AllArgsConstructor
public class RegisterReservationUseCase {

    private final ReservationValidator reservationValidator;
    private final ReservationBuilder reservationBuilder;
    private final ReservationRepository reservationRepository;

    private final EquipmentRepository equipmentRepository;

    public void execute(RegisterReservationDto registerReservation, Integer userId) {
        reservationValidator.validate(registerReservation);

        var optEquipment = equipmentRepository.findById(registerReservation.getEquipment());
        if (optEquipment.isEmpty()) {
            throw new IllegalArgumentException(MESSAGE_INVALID_EQUIPMENT);
        }

        var optReservation = reservationRepository.findByDateAndEquipment(
                registerReservation.getEquipment(), registerReservation.getDate());
        if (optReservation.isPresent()) {
            throw new IllegalArgumentException(MESSAGE_EQUIPMENT_ALREADY_RESERVED);
        }

        var reservation = reservationBuilder.from(registerReservation, userId);
        reservationRepository.save(reservation);
    }

}
