package br.edu.unisep.techmanager.domain.validator;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ValidationMessages {

    public static final String MESSAGE_REQUIRED_EQUIPMENT = "Equipamento inválido!";
    public static final String MESSAGE_INVALID_EQUIPMENT = "Equipamento inválido!";
    public static final String MESSAGE_EQUIPMENT_ALREADY_RESERVED = "Equipamento já está reservado neste dia!";
    public static final String MESSAGE_REQUIRED_RESERVATION_DATE = "Data da reserva obrigatória!";
    public static final String MESSAGE_INVALID_RESERVATION_DATE = "Data deve ser igual ou superior ao dia atual!";

    public static final String MESSAGE_INVALID_EQUIPMENT_TYPE = "Tipo de equipamento inválido!";
    public static final String MESSAGE_INVALID_EQUIPMENT_NAME = "Nome de equipamento inválido!";

}
