package br.edu.unisep.techmanager.domain.validator.equipment;

import br.edu.unisep.techmanager.domain.dto.equipment.RegisterEquipmentDto;
import org.springframework.stereotype.Component;

import static br.edu.unisep.techmanager.domain.validator.ValidationMessages.MESSAGE_INVALID_EQUIPMENT_NAME;
import static br.edu.unisep.techmanager.domain.validator.ValidationMessages.MESSAGE_INVALID_EQUIPMENT_TYPE;
import static org.apache.commons.lang3.Validate.isTrue;
import static org.apache.commons.lang3.Validate.notNull;

@Component
public class EquipmentValidator {

    public void validate(RegisterEquipmentDto equipment) {
        notNull(equipment.getName(), MESSAGE_INVALID_EQUIPMENT_NAME);
        notNull(equipment.getType(), MESSAGE_INVALID_EQUIPMENT_TYPE);

        isTrue(equipment.getType() != 0, MESSAGE_INVALID_EQUIPMENT_TYPE);
    }
}
