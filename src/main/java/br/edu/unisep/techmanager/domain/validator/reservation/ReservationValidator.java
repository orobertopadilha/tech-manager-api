package br.edu.unisep.techmanager.domain.validator.reservation;

import br.edu.unisep.techmanager.domain.dto.reservation.RegisterReservationDto;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

import static br.edu.unisep.techmanager.domain.validator.ValidationMessages.*;
import static org.apache.commons.lang3.Validate.isTrue;
import static org.apache.commons.lang3.Validate.notNull;

@Component
public class ReservationValidator {

    public void validate(RegisterReservationDto reservation) {

        notNull(reservation.getEquipment(), MESSAGE_REQUIRED_EQUIPMENT);
        notNull(reservation.getDate(), MESSAGE_REQUIRED_RESERVATION_DATE);

        var today = LocalDate.now();
        isTrue(!reservation.getDate().isBefore(today), MESSAGE_INVALID_RESERVATION_DATE);
    }

}
